import re
import csv
import glob
from datetime import datetime
from dataclasses import dataclass
from collections import defaultdict
import logging

re_time = re.compile(r"""(?P<day>\d+).
                         (?P<month>\d+).
                         (?P<year>\d{4})\s
                         (?P<hour>\d+)""", re.VERBOSE)

re_identity = re.compile(r"""https?://[-.0-9a-z]+           # pass hostname
                            /(?P<service>zif-udl-rtdwebapi  # possible choices
                                         |zif-udl-dfawebapi
                                         |zif-udl-mdswebapi)
                            /restapi/(?P<controller>[A-z]+) # e.g. RawData, SubscriptionData)
                            (/[-.0-9a-z]+)?                 # pass uid if exists
                            /(?P<type>\w+)""", re.VERBOSE)

@dataclass
class Request:
    _dt: datetime
    _service: str
    _controller: str
    _type: str

data = {} 
# look like
# { year : {
#            month : {
#                    day : {
#                        hour : {
#                             service: {
#                                   controller : {
#                                               type : 666 #here is counter
#                                               }
#                                   }
#                             }
#                        }
#                    }
#            }
# }
def aggregate(req):
    data.setdefault(req._dt.year, {}) \
            .setdefault(req._dt.month, {}) \
            .setdefault(req._dt.day, {}) \
            .setdefault(req._dt.hour, {}) \
            .setdefault(req._service, {}) \
            .setdefault(req._controller, defaultdict(int)) \
            [req._type] += 1

def sv_data():
    with open("aggr.csv", "w") as fl:
        wr = csv.writer(fl)

        for year in sorted(data.keys()):
            d_m = data[year]
            for month in sorted(d_m.keys()):
                d_d = d_m[month]
                for day in sorted(d_d.keys()):
                    d_h = d_d[day]
                    for hour in sorted(d_h.keys()):
                        d_s = d_h[hour]
                        for service in d_s:
                            d_c = d_s[service]
                            for controller in d_c:
                                d_t = d_c[controller]
                                for tp in d_t:
                                    counter = d_t[tp]
                                    wr.writerow(['{day}-{month}-{year} {hour}'.format(day=day,month=month,year=year,hour=hour)] + 
                                                [service] + [controller] + [tp] + [counter])

def save():
    print('writting aggregation data to aggr.csv')
    sv_data()
    print('the filed was saved')

def pr_values(line):
    mc_dt = re_time.search(line)
    if mc_dt is None:
        logging.error("Couldn't find datetime")
        return

    dt = datetime(year=int(mc_dt.group("year")),
            month=int(mc_dt.group("month")),
            day=int(mc_dt.group("day")),
            hour=int(mc_dt.group("hour")))

    mc_id = re_identity.search(line)
    if mc_id is None:
        logging.error("Couldn't find service")
        return

    req = Request(_dt=dt,
            _service=mc_id.group("service"),
            _controller=mc_id.group("controller"),
            _type=mc_id.group("type"))

    aggregate(req)

def pr_line(line):
    sieve1 = re.search("Request (starting|finished)", line)
    if sieve1 is not None:
        logging.debug('Found a request: %s', line)
        logging.debug('Match is: %s', sieve1.groups())
        pr_values(line)


def pr_file(flname):
    with open(flname) as fl:
        for line in fl:
            pr_line(line)

def parse(flname):
    print('starting %s!' % flname)
    pr_file(flname)
    print('file parsed')

def files():
    fs = glob.glob('*.csv')    
    return fs

def main():
    logging.basicConfig(filename='lp.log', filemode='w', level=logging.DEBUG, force=True)
    listOfFiles = files()
    for f in listOfFiles:
        if f == 'aggr.csv':
            continue
        parse(f)
    save()

if __name__=="__main__":
    main()
